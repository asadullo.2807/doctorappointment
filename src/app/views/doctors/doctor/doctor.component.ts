import { Component, OnInit } from '@angular/core';
import { DoctorsService } from '../doctors.service'
@Component({
  selector: 'doctor',
  templateUrl: 'doctor.component.html',
  styleUrls: ['./doctor.component.scss']
})
export class DoctorComponent implements OnInit {
  doctors: any = [];
  constructor(private doctorSvc: DoctorsService) { }

   ngOnInit(): void {
    this.doctors = this.doctorSvc.getDoctors();
  }

}
