import { Injectable } from '@angular/core';

interface IDoctor {
  id: string,
  name: string,
  position: string,
  address: string,
  medicalPlace: string,
  speciality: string
  image: string,
  rating: number
}
@Injectable({
  providedIn: 'root'
})
export class DoctorsService {


  constructor() { }

  public doctors: IDoctor[]= [
    {
      name: 'Sardor Xoshimjanov',
      id: '628fa9bca841da5062095162',
      position: 'Vrach lor',
      address: 'Namangan viloyati Uychi tumani Jiydakapa qishlog\'i ,12',
      medicalPlace: 'Uychi tumani tibbiyot birlashmasi',
      speciality: 'Umumiy shifokor',
      image: '../../../../assets/images/doctor1.jpg',
      rating: 4
    },
    {
      name: 'Ukaxara Takaxara',
      id: '628faacba841da5062095165',
      position: 'Vrach endiokrinolog',
      address: 'Namangan viloyati Uychi tumani Jiydakapa qishlog\'i ,12',
      medicalPlace: 'Uychi tumani tibbiyot birlashmasi',
      speciality: 'Endokrinolog',
      image: '../../../../assets/images/doctor2.jpg',
      rating: 4.2
    },
    {
      name: 'Burak Yilmaz',
      id: '628fab04a841da5062095166',
      position: 'Vrach nervpatolog',
      address: 'Namangan viloyati Uychi tumani Jiydakapa qishlog\'i ,12',
      medicalPlace: 'Uychi tumani tibbiyot birlashmasi',
      speciality: 'Nervpatolog',
      image: '../../../../assets/images/doctor3.jpg',
      rating: 3.5
    },
    {
      name: 'Messi Chichqoq',
      id: '628fab41865bffba2c0a6b76',
      position: 'Jinni',
      address: 'Argentina Respublikasi Messi chichqoq tumani Messi mol qishlog\'i ,10',
      medicalPlace: 'Uychi tumani tibbiyot birlashmasi',
      speciality: 'Odamlarni o\'ziga o\'xshab jinni qilish',
      image: '../../../../assets/images/doctor4.jpeg',
      rating: 0
    },
    {
      name: 'Ronaldu Mujik',
      id: '628faba2865bffba2c0a6b77',
      position: 'Neyroxirurg',
      address: 'Portugaliya Respublikasi Cristiano tumani Aveiro qishlog\'i , 7',
      medicalPlace: 'Uychi tumani tibbiyot birlashmasi',
      speciality: 'Umumiy shifokor (neyroxirurg)',
      image: '../../../../assets/images/doctor5.jpg',
      rating: 5
    },
    {
      name: 'Neymar Junior Eshshak',
      id: '628fac46865bffba2c0a6b78',
      position: 'Vrach eshshak',
      address: 'Braziliya Respublikasi Neymar chichqoq tumani Neymar mol qishlog\'i ,10',
      medicalPlace: 'Uychi tumani tibbiyot birlashmasi',
      speciality: 'Mol',
      image: '../../../../assets/images/doctor6.jpeg',
      rating: 0.1
    },
    {
      name: 'Play Station',
      id: '628fac53865bffba2c0a6b79',
      position: 'Vrach lor',
      address: 'Namangan viloyati Uychi tumani Jiydakapa qishlog\'i ,12',
      medicalPlace: 'Uychi tumani tibbiyot birlashmasi',
      speciality: 'Umumiy shifokor',
      image: '../../../../assets/images/doctor7.png',
      rating: 4.2
    }
  ];

  public getDoctors() {
    return this.doctors;     
  }
  public getDoctorById(id: string) {
    return this.doctors.find(doctor => doctor.id == id);
  }
}
