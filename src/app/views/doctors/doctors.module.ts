import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardModule, GridModule,TableModule, UtilitiesModule   } from '@coreui/angular';

import { DoctorComponent } from './doctor/doctor.component';
import { DoctorsRoutingModule } from './doctors-routing.module';
import { DoctorInfoComponent } from './doctor-info/doctor-info.component';


@NgModule({
  declarations: [
    DoctorComponent,
    DoctorInfoComponent
  ],
  imports: [
    CommonModule,
    DoctorsRoutingModule,
    CardModule,
    GridModule,
    TableModule,
    UtilitiesModule
  ]
})
export class DoctorsModule { }
