import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DoctorsService } from '../doctors.service';
@Component({
  selector: 'app-doctor-info',
  templateUrl: './doctor-info.component.html',
  styleUrls: ['./doctor-info.component.scss']
})
export class DoctorInfoComponent implements OnInit {
  selectedDoctor: any = {};
  heroes: any = [1,2,3,4,5,6,7,8,9]
  queryDoctorId: any = '';
  constructor(
    private route: ActivatedRoute,
    private doctorService: DoctorsService
    ) { }

  ngOnInit(): void {
   this.route.queryParamMap.subscribe(queryParams => {
     this.queryDoctorId = queryParams.get('doctor_id');
     this.getDoctorById(this.queryDoctorId);
   })
  }
  getDoctorById(queryDoctorId: any) {
    this.selectedDoctor = this.doctorService.getDoctorById(queryDoctorId);
    console.log(this.selectedDoctor);
  }

}
